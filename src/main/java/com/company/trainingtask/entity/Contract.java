package com.company.trainingtask.entity;

import io.jmix.core.DeletePolicy;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.OnDeleteInverse;
import io.jmix.core.metamodel.annotation.Composition;
import io.jmix.core.metamodel.annotation.DependsOnProperties;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.webdav.entity.WebdavDocument;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@JmixEntity
@Table(name = "CONTRACT")
@Entity
public class Contract {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @NotNull
    @OnDeleteInverse(DeletePolicy.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CUSTOMER_ID", nullable = false)
    private Organization customer;

    @OnDeleteInverse(DeletePolicy.CASCADE)
    @JoinColumn(name = "PERFORMER_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Organization performer;

    @Column(name = "NUMBER_", nullable = false)
    @NotNull
    private Integer number;

    @Column(name = "SIGNED_DATE", nullable = false)
    @NotNull
    private LocalDate signedDate;

    @Column(name = "TYPE_")
    private String type;

    @Column(name = "DATE_FROM", nullable = false)
    @NotNull
    private LocalDate dateFrom;

    @Column(name = "DATE_TO", nullable = false)
    @NotNull
    private LocalDate dateTo;

    @NotNull
    @Column(name = "AMOUNT", nullable = false)
    private Integer amount;

    @Column(name = "VAT", nullable = false)
    @NotNull
    private Integer vat;

    @Column(name = "TOTAL_AMOUNT")
    private Integer totalAmount;

    @Column(name = "CUSTOMER_SIGNER", nullable = false)
    @NotNull
    private String customerSigner;

    @Column(name = "PERFORMER_SIGNER", nullable = false)
    @NotNull
    private String performerSigner;

    @Column(name = "STATUS")
    private Integer status;

    @OneToMany(mappedBy = "contract")
    @Composition
    private List<Stage> stages;

    @JoinTable(name = "CONTRACT_WEBDAV_DOCUMENT_LINK",
            joinColumns = @JoinColumn(name = "CONTRACT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "WEBDAV_DOCUMENT_ID", referencedColumnName = "ID"))
    @ManyToMany
    private List<WebdavDocument> files;

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public List<WebdavDocument> getFiles() {
        return files;
    }

    public void setFiles(List<WebdavDocument> files) {
        this.files = files;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public StatusModel getStatus() {
        return status == null ? null : StatusModel.fromId(status);
    }

    public void setStatus(StatusModel status) {
        this.status = status == null ? null : status.getId();
    }

    public String getPerformerSigner() {
        return performerSigner;
    }

    public void setPerformerSigner(String performerSigner) {
        this.performerSigner = performerSigner;
    }

    public String getCustomerSigner() {
        return customerSigner;
    }

    public void setCustomerSigner(String customerSigner) {
        this.customerSigner = customerSigner;
    }

    public Integer getVat() {
        return vat;
    }

    public void setVat(Integer vat) {
        this.vat = vat;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Type getType() {
        return type == null ? null : Type.fromId(type);
    }

    public void setType(Type type) {
        this.type = type == null ? null : type.getId();
    }

    public LocalDate getSignedDate() {
        return signedDate;
    }

    public void setSignedDate(LocalDate signedDate) {
        this.signedDate = signedDate;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Organization getPerformer() {
        return performer;
    }

    public void setPerformer(Organization performer) {
        this.performer = performer;
    }

    public Organization getCustomer() {
        return customer;
    }

    public void setCustomer(Organization customer) {
        this.customer = customer;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @InstanceName
    @DependsOnProperties({"number"})
    public String getInstanceName() {
        return String.format("Contract №%s", number);
    }
}