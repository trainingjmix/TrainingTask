package com.company.trainingtask.entity;

import io.jmix.search.index.annotation.AutoMappedField;
import io.jmix.search.index.annotation.JmixEntitySearchIndex;

@JmixEntitySearchIndex(entity = Contract.class)
public interface ContractIndexDefinition {

    @AutoMappedField(includeProperties = {"customer", "performer", "number", "signedDate", "type", "dateFrom",
                                        "dateTo", "amount", "vat", "totalAmount", "customerSigner", "performerSigner",
                                        "status", "stages"})
    void contractMapping();
    
}
