package com.company.trainingtask.entity;

import io.jmix.search.index.annotation.AutoMappedField;
import io.jmix.search.index.annotation.JmixEntitySearchIndex;

@JmixEntitySearchIndex(entity = Organization.class)
public interface OrganizationIndexDefinition {

    @AutoMappedField(includeProperties = {"name", "taxNumber", "registrationNumber", "escapeVat"})
    void organizationMapping();

}
