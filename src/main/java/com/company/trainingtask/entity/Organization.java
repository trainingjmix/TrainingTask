package com.company.trainingtask.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@JmixEntity
@Table(name = "ORGANIZATION", uniqueConstraints = {
        @UniqueConstraint(name = "IDX_ORGANIZATION_UNQ_NAME", columnNames = {"NAME"}),
        @UniqueConstraint(name = "IDX_ORGANIZATION_UNQ_TAXNUMBER", columnNames = {"TAX_NUMBER"}),
        @UniqueConstraint(name = "IDX_ORGANIZATION_UNQ_REGNUMBER", columnNames = {"REGISTRATION_NUMBER"})
})
@Entity
public class Organization {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @InstanceName
    @Column(name = "NAME", nullable = false)
    @NotNull
    private String name;

    @Column(name = "TAX_NUMBER", nullable = false)
    @NotNull
    private Integer taxNumber;

    @Column(name = "REGISTRATION_NUMBER", nullable = false)
    @NotNull
    private Integer registrationNumber;

    @Column(name = "ESCAPE_VAT", nullable = false)
    @NotNull
    private Boolean escapeVat = false;

    public void setEscapeVat(Boolean escapeVat) {
        this.escapeVat = escapeVat;
    }

    public Boolean getEscapeVat() {
        return escapeVat;
    }

    public Integer getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(Integer registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Integer getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(Integer taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}