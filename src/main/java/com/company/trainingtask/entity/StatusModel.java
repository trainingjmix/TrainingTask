package com.company.trainingtask.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum StatusModel implements EnumClass<Integer> {

    NEW(10),
    TO_BE_AGREED(20),
    ACTIVE(30),
    COMPLETED(40),
    CANCELED(50);

    private Integer id;

    StatusModel(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static StatusModel fromId(Integer id) {
        for (StatusModel at : StatusModel.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}