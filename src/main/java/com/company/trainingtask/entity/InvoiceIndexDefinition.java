package com.company.trainingtask.entity;

import io.jmix.search.index.annotation.AutoMappedField;
import io.jmix.search.index.annotation.JmixEntitySearchIndex;

@JmixEntitySearchIndex(entity = Invoice.class)
public interface InvoiceIndexDefinition {

    @AutoMappedField(includeProperties = {"number", "date", "amount", "vat", "totalAmount", "description", "files", "stage"})
    void invoiceMapping();

}
