package com.company.trainingtask.entity;

import io.jmix.search.index.annotation.AutoMappedField;
import io.jmix.search.index.annotation.JmixEntitySearchIndex;

@JmixEntitySearchIndex(entity = Stage.class)
public interface StageIndexDefinition {

    @AutoMappedField(includeProperties = {"contract", "name", "dateFrom", "dateTo", "amount", "vat", "totalAmount", "description"})
    void stageMapping();

}
