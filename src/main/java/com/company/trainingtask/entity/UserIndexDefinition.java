package com.company.trainingtask.entity;

import io.jmix.search.index.annotation.AutoMappedField;
import io.jmix.search.index.annotation.JmixEntitySearchIndex;

@JmixEntitySearchIndex(entity = User.class)
public interface UserIndexDefinition {

    @AutoMappedField(includeProperties = {"username", "password", "firstName", "lastName", "email", "active"})
    void userMapping();

}
