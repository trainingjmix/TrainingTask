package com.company.trainingtask.entity;

import io.jmix.search.index.annotation.AutoMappedField;
import io.jmix.search.index.annotation.JmixEntitySearchIndex;

@JmixEntitySearchIndex(entity = ServiceCompletionCertificate.class)
public interface ServiceCompletionCertificateIndexDefinition {

    @AutoMappedField(includeProperties = {"number", "date", "amount", "vat", "totalAmount", "description", "files", "stage"})
    void serviceCompletionCertificateMapping();

}
