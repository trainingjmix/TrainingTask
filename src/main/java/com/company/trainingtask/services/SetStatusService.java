package com.company.trainingtask.services;

import com.company.trainingtask.entity.Contract;
import com.company.trainingtask.entity.StatusModel;
import io.jmix.core.DataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component()
public class SetStatusService {
    @Autowired
    private DataManager dataManager;

    public void setStatus(Contract contract, String status) {
        switch (status) {
            case "New" -> contract.setStatus(StatusModel.NEW);
            case "To be agreed" -> contract.setStatus(StatusModel.TO_BE_AGREED);
            case "Active" -> contract.setStatus(StatusModel.ACTIVE);
            case "Completed" -> contract.setStatus(StatusModel.COMPLETED);
            case "Canceled" -> contract.setStatus(StatusModel.CANCELED);
        }
        dataManager.save(contract);
    }

}
