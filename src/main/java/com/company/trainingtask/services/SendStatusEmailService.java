package com.company.trainingtask.services;

import com.company.trainingtask.entity.Contract;
import com.company.trainingtask.entity.User;
import io.jmix.bpm.engine.events.ActivityCompletedEvent;
import io.jmix.core.DataManager;
import io.jmix.email.EmailInfo;
import io.jmix.email.EmailInfoBuilder;
import io.jmix.email.Emailer;
import org.flowable.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Component()
public class SendStatusEmailService {
    @Autowired
    private Emailer emailer;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private JavaMailSender emailSender;

    @Bean
    public void getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("my.gmail@gmail.com");
        mailSender.setPassword("password");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        emailSender = mailSender;
    }
    @EventListener
    public void onSetContractStatusCompleted(ActivityCompletedEvent event) {
        getJavaMailSender();
        if ("setContractStatus".equals(event.getActivityName())) {
            Contract contract = (Contract) runtimeService.getVariable(event.getExecutionId(), "contract");
            User userManager = (User) runtimeService.getVariable(event.getExecutionId(), "manager");
            User userInitiator = (User) runtimeService.getVariable(event.getExecutionId(), "initiator");
            List<User> processUsers = new ArrayList<>();
            processUsers.add(userInitiator);
            processUsers.add(userManager);
            String emailTitle = "Status of the contract has been changed";
            SimpleMailMessage message = new SimpleMailMessage();
            for (User user : processUsers) {
                String emailBody = "Hi " + user.getUsername() + "\n" +
                        "Status of the contract \"" + contract.getInstanceName() + "\" has been changed to " + contract.getStatus();
                message.setFrom("noreply@baeldung.com");
                message.setTo(user.getEmail());
                message.setSubject(emailTitle);
                message.setText(emailBody);
                emailSender.send(message);

                /*EmailInfo emailInfo = EmailInfoBuilder.create()
                        .setAddresses(user.getEmail())
                        .setSubject(emailTitle)
                        .setFrom(null)
                        .setBody(emailBody)
                        .build();
                emailer.sendEmailAsync(emailInfo);*/
            }
        }
    }
}
