package com.company.trainingtask.screen.stage;

import io.jmix.ui.screen.*;
import com.company.trainingtask.entity.Stage;

@UiController("Stage.edit")
@UiDescriptor("stage-edit.xml")
@EditedEntityContainer("stageDc")
public class StageEdit extends StandardEditor<Stage> {
}