package com.company.trainingtask.screen.servicecompletioncertificate;

import io.jmix.ui.screen.*;
import com.company.trainingtask.entity.ServiceCompletionCertificate;

@UiController("ServiceCompletionCertificate.browse")
@UiDescriptor("service-completion-certificate-browse.xml")
@LookupComponent("serviceCompletionCertificatesTable")
public class ServiceCompletionCertificateBrowse extends StandardLookup<ServiceCompletionCertificate> {
}