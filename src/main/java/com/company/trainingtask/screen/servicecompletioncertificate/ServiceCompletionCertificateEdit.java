package com.company.trainingtask.screen.servicecompletioncertificate;

import io.jmix.ui.screen.*;
import com.company.trainingtask.entity.ServiceCompletionCertificate;

@UiController("ServiceCompletionCertificate.edit")
@UiDescriptor("service-completion-certificate-edit.xml")
@EditedEntityContainer("serviceCompletionCertificateDc")
public class ServiceCompletionCertificateEdit extends StandardEditor<ServiceCompletionCertificate> {
}