package com.company.trainingtask.screen.contract;

import com.company.trainingtask.entity.*;
import io.jmix.appsettings.AppSettings;
import io.jmix.core.DataManager;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.Label;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.CollectionPropertyContainer;
import io.jmix.ui.model.DataContext;
import io.jmix.ui.screen.*;
import io.jmix.webdav.entity.WebdavDocument;
import io.jmix.webdav.service.WebdavDocumentsManagementService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

@UiController("Contract.edit")
@UiDescriptor("contract-edit.xml")
@EditedEntityContainer("contractDc")
public class ContractEdit extends StandardEditor<Contract> {
    @Autowired
    private AppSettings appSettings;
    @Autowired
    private WebdavDocumentsManagementService webdavDocumentsManagementService;
    @Autowired
    private CollectionPropertyContainer<WebdavDocument> filesDc;
    @Subscribe
    public void onInitEntity(InitEntityEvent<Contract> event) {
        TrainingTaskSettings trainingTaskSettings = appSettings.load(TrainingTaskSettings.class);
        Contract contract = event.getEntity();
        contract.setStatus(StatusModel.NEW);
        contract.setVat(trainingTaskSettings.getVat());
    }

    @Subscribe("commitAndCloseBtn")
    public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
        TrainingTaskSettings trainingTaskSettings = appSettings.load(TrainingTaskSettings.class);
        Contract contract = getEditedEntity();
        if (contract.getPerformer().getEscapeVat() || contract.getCustomer().getEscapeVat()) {
            contract.setVat(0);
        }
        else {
            contract.setVat(trainingTaskSettings.getVat());
        }
        contract.setTotalAmount(contract.getAmount() + contract.getVat());
        if (stagesDc.getItems().size() == 0) {
             Stage newStage = dataContext.create(Stage.class);
             newStage.setName("Stage0");
             newStage.setContract(contract);
             newStage.setDateFrom(contract.getDateFrom());
             newStage.setDateTo(contract.getDateTo());
             newStage.setAmount(contract.getAmount());
             newStage.setVat(contract.getVat());
             newStage.setTotalAmount(contract.getTotalAmount());
             dataContext.commit();
             stagesDc.getMutableItems().add(newStage);
         }
    }

    @Autowired
    private CollectionPropertyContainer<Stage> stagesDc;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private DataContext dataContext;
    @Autowired
    private UiComponents uiComponents;
    @Autowired
    private CollectionContainer<Invoice> invoicesDc;
    @Autowired
    private CollectionLoader<Invoice> invoicesDl;
    @Autowired
    private CollectionContainer<ServiceCompletionCertificate> serviceCompletionCertificatesDc;
    @Autowired
    private CollectionLoader<ServiceCompletionCertificate> serviceCompletionCertificatesDl;
    @Autowired
    private Notifications notifications;

    @Subscribe
    public void onAfterInit(AfterInitEvent event) {

    }

    @Install(to = "stagesTable.vat", subject = "columnGenerator")
    private Component stagesTableVatColumnGenerator(Stage stage) {
        Label label = uiComponents.create(Label.class);
        TrainingTaskSettings trainingTaskSettings = appSettings.load(TrainingTaskSettings.class);
        if (stage.getContract().getCustomer().getEscapeVat() || stage.getContract().getPerformer().getEscapeVat()) {
            stage.setVat(0);
        }
        else {
            stage.setVat(trainingTaskSettings.getVat());
        }
        label.setValue(stage.getVat());
        return label;
    }

    @Install(to = "stagesTable.totalAmount", subject = "columnGenerator")
    private Component stagesTableTotalAmountColumnGenerator(Stage stage) {
        Label label = uiComponents.create(Label.class);
        stage.setTotalAmount(stage.getAmount() + stage.getVat());
        label.setValue(stage.getTotalAmount());
        return label;
    }

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        Contract contract = getEditedEntity();
        invoicesDl.setParameter("stageContract", contract);
        serviceCompletionCertificatesDl.setParameter("stageContract", contract);
        invoicesDl.load();
        serviceCompletionCertificatesDl.load();
    }

    @Subscribe("invoicesTable1GenerateBtn")
    public void onInvoicesTable1GenerateBtnClick(Button.ClickEvent event) {
        int i = 1;
        for (Stage stage : stagesDc.getItems()) {
            if (invoicesDc.getItems().stream().noneMatch(Invoice -> Invoice.getStage().equals(stage))) {
                Invoice invoice = dataContext.create(Invoice.class);
                invoice.setNumber(i++);
                invoice.setAmount(stage.getAmount());
                invoice.setDate(LocalDate.now());
                invoice.setVat(stage.getVat());
                invoice.setTotalAmount(stage.getTotalAmount());
                invoice.setDescription(stage.getDescription());
                invoice.setStage(stage);
                invoicesDc.getMutableItems().add(invoice);
            }
        }
    }

    @Subscribe("serviceCompletionCertificatesTableGenerateBtn")
    public void onServiceCompletionCertificatesTableGenerateBtnClick(Button.ClickEvent event) {
        int i = 1;
        for (Stage stage : stagesDc.getItems()) {
            if (serviceCompletionCertificatesDc.getItems().stream()
                    .noneMatch(serviceCompletionCertificatesDc -> serviceCompletionCertificatesDc.getStage().equals(stage))) {
                ServiceCompletionCertificate serviceCompletionCertificate = dataContext.create(ServiceCompletionCertificate.class);
                serviceCompletionCertificate.setNumber(i++);
                serviceCompletionCertificate.setAmount(stage.getAmount());
                serviceCompletionCertificate.setDate(LocalDate.now());
                serviceCompletionCertificate.setVat(stage.getVat());
                serviceCompletionCertificate.setTotalAmount(stage.getTotalAmount());
                serviceCompletionCertificate.setDescription(stage.getDescription());
                serviceCompletionCertificate.setStage(stage);
                serviceCompletionCertificatesDc.getMutableItems().add(serviceCompletionCertificate);
            }
        }
    }

}