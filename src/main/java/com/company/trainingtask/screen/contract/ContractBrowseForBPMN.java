package com.company.trainingtask.screen.contract;

import com.company.trainingtask.entity.StatusModel;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.trainingtask.entity.Contract;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("ContractBrowseForBPMN.browse")
@UiDescriptor("contract-browse-for-bpmn.xml")
@LookupComponent("contractsTable")
public class ContractBrowseForBPMN extends StandardLookup<Contract> {
    @Autowired
    private CollectionLoader<Contract> contractsDl;
    @Autowired
    private CollectionContainer<Contract> contractsDc;

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        contractsDl.setParameter("status", StatusModel.NEW);
        contractsDl.load();
    }
}