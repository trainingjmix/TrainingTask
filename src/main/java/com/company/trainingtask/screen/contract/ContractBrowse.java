package com.company.trainingtask.screen.contract;

import com.company.trainingtask.entity.TrainingTaskSettings;
import io.jmix.appsettings.AppSettings;
import io.jmix.core.DataManager;
import io.jmix.ui.UiComponents;
import io.jmix.ui.component.CheckBox;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.Label;
import io.jmix.ui.component.TextField;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.DataContext;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.trainingtask.entity.Contract;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@UiController("Contract.browse")
@UiDescriptor("contract-browse.xml")
@LookupComponent("contractsTable")
public class ContractBrowse extends StandardLookup<Contract> {
    @Autowired
    private CollectionContainer<Contract> contractsDc;
    @Autowired
    private UiComponents uiComponents;
    @Autowired
    private AppSettings appSettings;
    @Autowired
    private DataContext dataContext;

    @Install(to = "contractsTable.totalAmount", subject = "columnGenerator")
    private Component contractsTableTotalAmountColumnGenerator(Contract contract) {
        contract.setTotalAmount(contract.getAmount() + contract.getVat());
        dataContext.commit();
        Label labelTotalAmount = uiComponents.create(Label.class);
        labelTotalAmount.setValue(contract.getTotalAmount());
        return labelTotalAmount;
    }

    @Install(to = "contractsTable.vat", subject = "columnGenerator")
    private Component contractsTableVatColumnGenerator(Contract contract) {
        Label label = uiComponents.create(Label.class);
        TrainingTaskSettings trainingTaskSettings = appSettings.load(TrainingTaskSettings.class);
        if (contract.getPerformer().getEscapeVat() || contract.getCustomer().getEscapeVat()) {
            contract.setVat(0);
        }
        else {
            contract.setVat(trainingTaskSettings.getVat());
        }
        dataContext.commit();
        label.setValue(contract.getVat());
        return label;
    }

}