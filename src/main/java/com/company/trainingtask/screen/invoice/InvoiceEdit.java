package com.company.trainingtask.screen.invoice;

import io.jmix.ui.screen.*;
import com.company.trainingtask.entity.Invoice;

@UiController("Invoice.edit")
@UiDescriptor("invoice-edit.xml")
@EditedEntityContainer("invoiceDc")
public class InvoiceEdit extends StandardEditor<Invoice> {
}