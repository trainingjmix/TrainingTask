package com.company.trainingtask.screen.contractchart;

import com.company.trainingtask.entity.Contract;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("ContractChartScreen")
@UiDescriptor("contract-chart-screen.xml")
public class ContractChartScreen extends Screen {
    @Autowired
    private CollectionContainer<Contract> contractsDc;
    @Autowired
    private CollectionLoader<Contract> contractsDl;

    @Subscribe
    public void onInit(InitEvent event) {

    }

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        contractsDl.load();
    }
    
    
}