package com.company.trainingtask.screen.search;

import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("SearchScreen")
@UiDescriptor("search-screen.xml")
public class SearchScreen extends Screen {
}